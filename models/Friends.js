const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const FriendSchema = new Schema ({
	name: String,
	gender: String,
	age: Number,
	description: String,
	status: String
});

module.exports = mongoose.model("Friends", FriendSchema);