//load mongoose to be able to use models

const mongoose = require("mongoose");

//use mongoose's schema for the models
const Schema = mongoose.Schema;

//create a new Schema for fruits
const PetSchema = new Schema({
	name: String,
	species: String,
	breed: String,
	description: String,
	age: Number		
}); 

//export the model so that other files can use it.
module.exports = mongoose.model("Pet", PetSchema);

