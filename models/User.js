const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const UserSchema = new Schema ({
	name: String,
	age: Number,
	isActive: Boolean,
	birthday: {
		year: Number,
		month: Number,
		day: Number
	}
});

module.exports = mongoose.model("User", UserSchema);