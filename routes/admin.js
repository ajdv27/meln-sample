onst express = require("express");

const router = express.Router();

const UserModel = require("../models/User.js");


router.get("/", function(req, res){
	UserModel.find({}, (err, user) => {
		if(!err){
			return res.json({"user": user});
		} else {
			console.log(err);
		}
	})
});