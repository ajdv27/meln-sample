//require Express and assign it to the variable called express
const express = require('express');

//call mongoose for handling multiple db connections

const mongoose = require("mongoose");

//call body parsin for json content types
const bodyParser = require("body-parser");

//establish a connection to the database:
//mongoose.connect("mongodb://localhost/sampledb");
mongoose.connect("mongodb+srv://database-admin:testing1234@sampledb-thman.mongodb.net/pets?retryWrites=true");

//pet model:
const PetModel = require("./models/Pet.js");

//require the fruit model
const FruitModel = require("./models/Fruit.js");

//Invoke express and assign the return value to the variable called app.
const app = express();

//save the value 3000 to the variable called port.
const port = 3000;

//use body parser in our application
app.use(bodyParser.json());

//in app, use the method called 'listen'
app.listen(port, () => console.log(`The server is listening to port ${port}`));

//get method
app.get('/', function (req, res, next) {
	//res.send('Hello World!')
	//res.send({data:1});
	next();
});

app.get('/', function (req, res) {
	//res.send('Hello World!')
	res.send({data:2});
});

//post request method
app.post('/', function (req, res) {
	res.send('Got a POST request')
});

app.put('/tuittcamper', function (req, res) {
	res.send('Got a PUT request from /tuittcamper!')
});

app.delete('/tuittcamper', function (req, res) {
	res.send('Got a DELETE request from /tuittcamper!')
});


//fruits route
app.get("/marketplace", function(req, res){
	FruitModel.find({}, (err, items) => {
		if(!err){
			return res.json({
				"items":items
			})
		} else {
			console.log(err);
		}
	});
});

app.get("/marketplace/:id", function(req, res){
	FruitModel.findOne({"_id" : req.params.id}) //req.params -> any wildcard we placed in the URL
	.then((fruit) => {
		if(fruit){
			return res.json({
				"result": fruit
			});
		} else {
			return res.send("No fruit found");
		}
	})
});

app.post("/fruit/add", function(req, res){
	//return res.send(req.body);
	FruitModel.create(req.body).then(function (result){
		return res.json(result);
	})
});

app.delete("/fruit/delete/:id", function(req, res){
	FruitModel.deleteOne({"_id":req.params.id})
	.then(fruit=> {
		if(fruit){
			return res.json({
				"fruit": fruit
			})
		}

		return res.json({
			"message":"No Fruit found"
		})
	})
});

app.put("/fruit/edit/:id", function(req, res){
	//id -> to  know what item to modify
	//body -> to know what data to modify with
	
	FruitModel.update({"_id":req.params.id}, req.body)
	.then(fruit => {
		if(fruit){
			return res.json({
				"result": fruit
			})
		}
		return res.json({"message": "no fruit found"});
	})
});

//pets route
app.get("/petplace", function(req, res){
	PetModel.find({}, (err, animals) => {
		if(!err){
			return res.json({
				"animals":animals
			})
		} else {
			console.log(err);
		}
	});
});

app.get("petplace/:id", function(req, res){
	PetModel.findOne({"_id" : req.params.id})
	.then((pet) => {
		if(pet){
			return res.json({
				"result": pet
			});
		} else {
			return res.send("No pet found");
		}
	})
});

app.post("/pet/add", function(req, res){
	PetModel.create(req.body).then(function (result){
		return res.json(result);
	})
});

app.put("/pet/edit/:id", function(req, res){
	PetModel.update({"_id":req.params.id}, req.body)
	.then(pet => {
		if(pet){
			return res.json({
				"result": pet
			})
		}
		return res.json({"message": "no pet found"});
	})
}); 

app.delete("/pet/delete/:id", function(req, res){
	PetModel.deleteOne({"_id":req.params.id})
	.then(pet=> {
		if(pet){
			return res.json({
				"pet": pet
			})
		}
		return res.json({
			"message": "No Pet found"
		})
	})
});

//friend routes
const friend = require("./routes/rentFriend");
app.use("/friend", friend);

//all the routes that have a word friend will look at the routes specified in rentFriend.js

//user route
const user = require("./routes/admin");
app.use("/admin", user);